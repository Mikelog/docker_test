#!/bin/sh
if [ ! -d "/var/run/mysqld" ]; then
        mkdir -p /var/run/mysqld
chown -R mysql.mysql /var/run/mysqld
fi
if [ -d /var/lib/mysql/mysql ]; then
  echo ' Mysql is here'
else
 chown -R mysql:mysql /var/lib/mysql
 mysql_install_db --user=mysql > /dev/null
 echo " MySql root password: $MYSQL_ROOT_PASSWORD "
 myinitfile=`mktemp`
 if [ ! -f "$myinitfile" ]; then
  return 1
 fi
 echo $MYSQL_ROOT_PWD > /tmp/MYSQL_ROOT_PWD
 cat  << EOF > $myinitfile 
USE mysql;
FLUSH PRIVILEGES;
DELETE FROM mysql.user;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '$MYSQL_ROOT_PWD' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '$MYSQL_ROOT_PWD' WITH GRANT OPTION;
EOF
  if [ "$MYSQL_DATABASE" != "" ]; then
   echo "[i] Creating database: $MYSQL_DATABASE"
   echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $myinitfile
   if [ "$MYSQL_TABLE" != ""]; then
    echo "USE \`$MYSQL_DATABASE\`';" > $myinitfile
    echo "CREATE TABLE IF NOT EXISTS \`$MYSQL_TABLE\`;" >>   $myinitfile
   fi  
   if [ "$MYSQL_USER" != "" ] && [ "$MYSQL_PASSWORD" != "" ]; then
    echo "GRANT ALL ON \`$MYSQL_DATABASE\`.* to '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';" >> $myinitfile
   fi
    else
     if [ "$MYSQL_USER" != "" ] && [ "$MYSQL_PASSWORD" != "" ]; then
      echo "GRANT ALL ON '$MYSQL_DATABASE'.* to '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';" >> $myinitfile
     fi
  fi
   echo 'FLUSH PRIVILEGES;' >> $myinitfile
   echo " run init "
   /usr/bin/mysqld --user=mysql --bootstrap --verbose=0 < $myinitfile
fi
sleep 5
exec /usr/bin/mysqld --user=mysql --console


